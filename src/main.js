import data from './data.js';
// console.log(data);
import Component from './components/Component.js';
// console.log(Component);
import Img from './components/Img.js';
// console.log(Img);
import PizzaThumbnail from './components/PizzaThumbnail.js';
import PizzaList from './components/PizzaList.js';
import Router from './Router.js';

// Titre
// const title = new Component('h1', null, 'La carte');
// document.querySelector('.pageTitle').innerHTML = title.render();
// const title2 = new Component('h1', null, ['La', ' ', 'carte']);
// document.querySelector('.pageTitle').innerHTML = title2.render();
// console.log(title2.render());

// Image
// const image = new Img(
// 	'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
// );
// document.querySelector('.pageContent').innerHTML = image.render();
// console.log(image.render());

// const c = new Component('article', { name: 'class', value: 'pizzaThumbnail' }, [
// 	new Img(
// 		'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
// 	),
// 	'Regina',
// ]);
// document.querySelector('.pageContent').innerHTML = c.render();
// console.log(c.render());

// PIZZA Thumbnail
// const pizza = data[0];
// const pizzaThumbnail = new PizzaThumbnail(pizza);
// document.querySelector('.pageContent').innerHTML = pizzaThumbnail.render();

// PIZZA List
// const pizzaList = new PizzaList(data);
// document.querySelector('.pageContent').innerHTML = pizzaList.render();
// console.log(pizzaList.render());

// Router
// const pizzaList = new PizzaList(data);
// Router.titleElement = document.querySelector('.pageTitle');
// Router.contentElement = document.querySelector('.pageContent');
// Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];
// Router.navigate('/');

// Private, getters / setters
const pizzaList = new PizzaList([]);

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas
