import Component from './components/Component';

class Router {
	static titleElement;
	static contentElement;
	static routes;

	static navigate(path) {
		this.routes.forEach(element => {
			if (element.path == path) {
				this.titleElement.innerHTML = new Component(
					'h1',
					null,
					element.title
				).render();
				this.contentElement.innerHTML = element.page.render();
			}
		});
	}
}

export default Router;
