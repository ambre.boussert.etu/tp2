import Component from './Component.js';
class Img extends Component {
	url;
	constructor(url) {
		super('img', { name: 'src', value: url });
	}
}
export default Img;
