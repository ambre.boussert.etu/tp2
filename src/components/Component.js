class Component {
	tagName;
	children;
	attribute;
	constructor(tagName, attribute, children) {
		this.attribute = attribute;
		this.tagName = tagName;
		this.children = children;
	}

	render() {
		if (!this.children) {
			return `<${this.tagName} ${renderAttribute(this.attribute)}"/>`;
		}

		if (!this.children && !this.attribute) {
			return `<${this.tagName}/>`;
		}

		if (!this.attribute) {
			return `<${this.tagName}>${renderChildren(this.children)}</${
				this.tagName
			}>`;
		}

		return `<${this.tagName} ${renderAttribute(
			this.attribute
		)}"> ${renderChildren(this.children)} </${this.tagName}>`;
	}
}
const renderAttribute = function (attribute) {
	return `${attribute.name}="${attribute.value}"`;
};
const renderChildren = function (children) {
	let res = '';
	if (children instanceof Array) {
		children.forEach(element => {
			if (element instanceof Component) {
				res += element.render();
			} else res += element;
		});
		return res;
	}
	return children;
};

export default Component;
