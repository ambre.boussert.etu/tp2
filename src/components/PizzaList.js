import Component from './Component';
import Img from './Img';
import data from '../data';
import PizzaThumbnail from './PizzaThumbnail';

class PizzaList extends Component {
	#pizzas = null;

	set pizzas(data) {
		console.log(data);
		this.#pizzas = data;
		super.children = data.map(pizza => new PizzaThumbnail(pizza));
	}

	constructor(data) {
		// MAP
		super(
			'section',
			{ name: 'class', value: 'pizzaList' },
			data.map(pizza => new PizzaThumbnail(pizza))
		);
	}
}

export default PizzaList;

// render() {
// 	if (this.#pizzas == null) {
// 		return super.render();
// 	} else {
// 		return this.#pizzas.render();
// 	}
// }
